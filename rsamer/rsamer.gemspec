require_relative 'lib/rsamer/version'

Gem::Specification.new do |spec|
  spec.name          = 'rsamer'
  spec.version       = Rsamer.version
  spec.date          = '2016-02-14'
  spec.summary       = 'Find similar users'
  spec.description   = 'This gem helps you to find similar users based on their estimates of a music genres'
  spec.authors       = [ "Alexey Spiridonov" ]
  spec.email         = 'alex9spiridonov@gmail.com'
  spec.homepage      = 'http://rubygems.org/gems/rsamer'
  spec.license       = 'MIT'

  spec.files         = [
    "lib/rsamer/data_provider.rb",
    "lib/rsamer/engine.rb",
    "lib/rsamer/user.rb",
    "lib/rsamer.rb",
    "lib/data/users.yml"
  ]

  spec.requirements = %w{
    Internet\ connection,
    A\ good\ mood
  }
end
