require 'yaml'
require 'active_support/all'
require_relative 'rsamer/data_provider'
require_relative 'rsamer/engine'

module Rsamer

  def self.find_for name
    engine.find_for name
  end

  def self.recommend name
    engine.recommend name
  end

  private
  def self.engine
    @engine ||= Engine.new(DataProvider.new.prepare)
  end
end
