module Rsamer
  class Engine

    RATING_THRESHOLD = 3

    attr_reader :users

    def initialize users
      @users = users
    end

    def find_for name
      @name = name
      @similars = {}
      return "User #{@name} in mot present in example. See  in the lib/data/users.yml" unless current_user

      @users.each do |user|
        next if user.name  == current_user.name
        @similars[user.name] = distance(current_user, user)
      end
      similar
    end

    def recommend name
      find_for name
      recommendations = []
      similar_user.genres.each do |genre|
        recommendations.push genre[0] unless genre[1] < RATING_THRESHOLD || current_user_have?(genre[0])
      end
      if recommendations.any?
        "We advise you to listen #{recommendations.join}"
      else
        "Sorry, but we can not advise you anything"
      end
    end

    private

    def current_user_have? genre
      current_user.genres.keys.find{ |user_genre| user_genre == genre }
    end

    def distance current_user, user
      distances = []

      current_user.genres.keys.each do |genre|
        next unless user.genres[genre]
        distances.push (current_user.genres[genre] - user.genres[genre])**2
      end

      1.fdiv 1 + Math.sqrt(distances.inject(0){ |sum, score| sum + score })
    end

    def current_user
      @current_user ||= @users.find{ |user| user.name == @name }
    end

    def similar_user
      @similar_user ||= @users.find{ |user| user.name == similar.first }
    end

    def similar
      @similars.max_by{ |_, score| score }
    end


  end
end
